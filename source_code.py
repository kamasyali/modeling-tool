import os
import pandas as pd
from matplotlib import pyplot
from scipy.optimize import minimize
import numpy as np

directory=(os.path.dirname(os.path.realpath(__file__)))
print(directory)

#Given assumptions
charge_capacity=100.0/1000
discharge_capacity=100.0/1000
discharge_energy_capacity=200.0/1000
efficiency=0.85
throughput=200.0/1000
zone=61761      #NYC

#Read data
i=0
for root,dirs,files in os.walk(directory):
	os.chdir(root)
	for file in files:
		if file.endswith(".csv"):
			if i==0:
				my_series = pd.read_csv(file,index_col=0)
			else:
				my_series=pd.concat([my_series,pd.read_csv(file,index_col=0)],sort=False)
			i += 1	
my_series=my_series.loc[my_series.PTID==zone]#selected NYC
my_series=my_series['LBMP ($/MWHr)']

#Initialization
Optimization_frame=24
last=0.0  #I assume battery level was zero before January 1st

def objective(x):
	return -np.sum(x*price)   #minimize the negative profit 


def calcstateofenergy(x):
	State_of_energy=np.zeros(Optimization_frame)
	for i in range(Optimization_frame):
		if i==0:
			if x[i]>0:  #discharging to the grid
				State_of_energy[i]=last-x[i]				
			else:
				State_of_energy[i]=last-x[i]*efficiency				
		else:
			if x[i]>0:  #discharging to the grid
				State_of_energy[i]=State_of_energy[(i-1)]-x[i]
			else:     #charging from the grid
				State_of_energy[i]=State_of_energy[(i-1)]-x[i]*efficiency
	return State_of_energy

def constraint1(x):
	return throughput-np.sum(x[x>0])   #maximum dailiy throughput constraint

def constraint2(x):
	State_of_energy=calcstateofenergy(x)
	return np.repeat(discharge_energy_capacity,Optimization_frame)-State_of_energy  #batter level cannot be more than discharge energy capacity

def constraint3(x):
	State_of_energy=calcstateofenergy(x)
	return State_of_energy-np.zeros(Optimization_frame)    #battery level cannot be less than zero



b=(-charge_capacity,discharge_capacity)
bnds=(b,) * Optimization_frame
con1={'type': 'ineq', 'fun': constraint1}
con2={'type': 'ineq', 'fun': constraint2}
con3={'type': 'ineq', 'fun': constraint3}
cons=[con1,con2,con3]


#initilization
final_x=np.zeros(len(my_series))
final_state_of_energy=np.zeros(len(my_series))
revenue=0
cost=0



for i in range(0,len(my_series),Optimization_frame): #do optimization for each day
	price=my_series.values[i:i+Optimization_frame]
	success=False
	while success==False or objective(sol.x)>0:  # optimization may not converge due to bad initial decision variables. #make sure it converges if not restart using different intial decision variables
		x0=np.random.rand(1,Optimization_frame)-0.5   #initial random variables
		sol=minimize(objective,x0,bounds=bnds,constraints=cons) 
		success=sol.success
		if success==True and objective(sol.x)<0:
			final_x[i:i+24]=sol.x  #power output
			final_state_of_energy[i:i+24]=calcstateofenergy(sol.x)  #state of energy (battery level)
			last=calcstateofenergy(sol.x)[-1] #battery level at last point. this value will be used next day the intial battery level
						

#Question 4
print("Power output",final_x*1000)
poweroutput=pd.DataFrame(data=final_x*1000, index= my_series.index, columns=['Power output(kW)'])
poweroutput.to_csv(directory+'\Power_output.csv')

print("State of energy", final_state_of_energy*1000)
battery_level=pd.DataFrame(data=final_state_of_energy*1000, index=my_series.index, columns=['State of energy(kWh)'])
battery_level.to_csv(directory+'\State_of_energy.csv')





#Question 5
revenue=np.sum(final_x[final_x>0]*my_series.values[final_x>0]) #total revenue
print("Total annual revenue generation is", revenue, "$")

cost=np.sum(final_x[final_x<0]*my_series.values[final_x<0]) # total cost
print("Total annual charging cost", -cost, "$")
total_throughput=np.sum(final_x[final_x>0])  #total throughput
print("Total annual discharged throughput", total_throughput*1000, "kWh")
#print("Annual profit is", revenue+cost, "$")

#Question 6
weeks=pd.to_datetime(my_series.index).weekofyear  #ISO week numbers
profit=final_x*my_series.values #hourly profits

#initialazation for real week number calculations
realweeks=np.zeros(len(my_series))  
j=0
week_profits=np.zeros(53) 
k=1
tmp=weeks[0]
ind=0

# a mini script to calculate real week numbers using ISO week numbers
for i in weeks:
	if i!=tmp:
		k=k+1
	tmp=i
	realweeks[ind]=k   #real week numbers (i.e., first day is not week 52)
	ind=ind+1

#weekly profit calculations
for i in realweeks:
	week_profits[int(i)-1]=profit[j]+week_profits[int(i)-1]
	j=j+1
	


maxpos = np.argmax(week_profits) #the most profitable week
print("The most profitable week is week", maxpos+1)### added 1 due because Python indexes start from 0



#plotting
x = np.arange(1, ((7*24)+1), 1)
fig, ax1 = pyplot.subplots()
ax1.set_xlabel('Time (h)')
ax1.set_ylabel('Dispatch (kW)',color='b')
ax1.plot(x,final_x[realweeks==maxpos+1]*1000, 'b-')  #multiply by 1000 to convert MW to kW
ax2 = ax1.twinx() 
ax2.set_ylabel('LBMP ($/MWHr)',color='g')  
ax2.plot(x,my_series.values[realweeks==maxpos+1], 'g-')
#fig.tight_layout()  
pyplot.show()


#initialization
month=pd.to_datetime(my_series.index).month
month_profits=np.zeros(len(np.unique(month)))


#montly profit calculations
j=0
for i in month:
	month_profits[int(i)-1]=profit[j]+month_profits[int(i)-1]
	j=j+1


#plotting
month_list=('Jan', 'Feb', 'Mar', 'April', 'May', 'June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec')
pyplot.bar(np.arange(len(np.unique(month))), month_profits)
pyplot.xticks(np.arange(len(np.unique(month))), month_list)
pyplot.ylabel('Profit ($)')
pyplot.show()

